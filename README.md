# Autism-friendly personal organizer for educational usage #
This project was made with the [Kivy Framework](http://kivy.org) for the [\#2014 Kivy App Contest](http://kivy.org/#contest).
The application is an autism-friendly personal organizer for educational usage.
It is designed for Windows, Linux and Android. In the future, it should work on Mac OS X and iOS too.
Although the deadline for the contest is set at May 15th 23:59 GMT, the active development of this project will continue.

## Features ##
* Every course has it's own colour
* Almost all text is available in different languages: Croatian, German, English, Spanish, French, Italian, Dutch,
  Portuguese, Swedish and Tagalog

## Nearby future features ##
* Plan your homework
* Icons for 'homework' types: make, learn, read, small test, large test, exam

## Future features ##
* Ability to turn on/off notification(s) per activity
* Synchronize your agenda on different devices with Dropbox

## Prerequisites ##
* Python 2.7.x with sqlite3 compile flag
* Kivy 1.8.0 or higher

## Installation instructions ##
* Clone this repository
* Enter the top directory
* Run `main.py`

## Technical details ##
### Modifications to the core ###
I had to modify parts of the kivy core to make the UI fit on phone screens in portrait mode. I was able to subclass
a lot of the core classes and modify them like that, however a couple of classes (mainly from kivy.uix.settings) were
difficult because of the super calls. I rewrote them adding minimal changes and used these instead of the core classes.
### Testcases ###
This application was tested on the following systems:
* Gentoo Linux (64 bit, kivy 1.8.0)
* Windows 7 Ultimate x64 (kivy 1.8.0)
* Android (Kivy launcher) (phones used: Sony Xperia Z and HTC One V)
* Android (Native) (phones used: Sony Xperia Z)

## First use instructions ##
On first use, a database will be created and initialized. This may take a while.
Tap the "more" menu in the right-hand corner of the screen. Choose "courses" and add the wanted
number of courses. Next, choose "schedule" from the same menu and fill in the schedule. On "close",
a window will popup with a timetable. Edit this table if necessary. On "close", another window will
popup, asking for the number of weeks this schedule is valid for, starting with the current week.
On the final "close" action, the organizer is displayed again.
The default colour of the courses can be changed via the "courses" settings. If a school day contains
more or less lessons, the default number of ten can be edited via the global settings, found at the
"more" menu -> "settings". A different language can be set as well via these settings as well.

## Special thanks to... ##
* My family for their (moral) support during this project.
* CrunchBang Community for their translations: Sector11 (Spanish) and iMBeCil (Croatian).
* I included her with 'my family', but my sister is mentioned again here: the Italian translation was provided by her
  and she mobilized a group of friends from all over the world via Tumblr to do more translations: German, French,
  Portuguese, Swedish and Tagalog. Furthermore, she helped with testing and debugging.