import kivy
kivy.require('1.8.0')

from kivy.app import App
from kivy.properties import NumericProperty, StringProperty, ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.settings import SettingSpacer
from kivy.uix.textinput import TextInput

from strings import TextData, cap_first_letter

__author__ = 'boisei0'

strings = TextData()


class Schedule(BoxLayout):
    edit_course_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(Schedule, self).__init__(**kwargs)

        self.app = App.get_running_app()
        self.rows = self.app.config_settings.getint('schedule', 'coursesaday')
        self.courses = self.app.dbh.get_all_courses_data_by_abbr()

        self.time_table_objects = dict()

        self.orientation = 'vertical'

        self._fill_schedule()

    def _fill_schedule(self):
        self.clear_widgets()

        top_row = BoxLayout(orientation='horizontal')
        top_row.add_widget(SettingSpacer())
        for i in range(1, 8):
            top_row.add_widget(Label(text='[b]{}[/b]'.format(cap_first_letter(_(strings.weekdays_abbr[i]))),
                               markup=True))
        self.add_widget(top_row)

        current_schedule = self.app.dbh.get_schedule_data()

        for i in range(self.rows):
            row = BoxLayout(orientation='horizontal', height='1cm')
            row.add_widget(Label(text='[b]{}[/b]'.format(i + 1), markup=True))
            for ii in range(7):
                key = '{}-{}'.format(ii, i)
                try:
                    self.time_table_objects[key] = Button(text=current_schedule[key].abbr,
                                                          on_release=self.edit_course)
                except (IndexError, KeyError):
                    self.time_table_objects[key] = Button(text='', on_release=self.edit_course)
                row.add_widget(self.time_table_objects[key])
            self.add_widget(row)

    def on_translate(self):
        self._fill_schedule()

    def edit_course(self, instance):
        self.edit_course_popup = Popup(title=cap_first_letter(_(strings.text['course'])), size_hint=(.4, .8))
        options = ['']
        options.extend(self.courses.keys())
        self.edit_course_popup.content = SelectCourseDialog(sorted(options))
        self.edit_course_popup.attach_to = instance
        self.edit_course_popup.open()


class SelectCourseDialog(BoxLayout):
    def __init__(self, options, **kwargs):
        super(SelectCourseDialog, self).__init__(**kwargs)
        self.orientation = 'vertical'

        for option in options:
            self.add_widget(Button(text=option, on_release=self._set_text))

        self.close_button_text = cap_first_letter(_(strings.text['close']))

    def _set_text(self, instance):
        self.parent.parent.parent.attach_to.text = instance.text
        self.parent.parent.parent.dismiss()


class ScheduleDialog(BoxLayout):
    app = App.get_running_app()
    rows = NumericProperty(10)
    close_button_text = StringProperty(u'Close')
    timetable_popup = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(ScheduleDialog, self).__init__(**kwargs)

    def on_translate(self):
        self.close_button_text = cap_first_letter(_(strings.text['close']))
        self.ids['schedule_layout'].__self__.on_translate()

    def on_close(self, *args, **kwargs):
        # check timetable
        self.timetable_popup = Popup(title=cap_first_letter(_(strings.text['schedule'])), auto_dismiss=False)
        self.timetable_popup.attach_to = self
        self.timetable_popup.content = TimeTable()
        self.timetable_popup.open()


class TimeTable(BoxLayout):
    def __init__(self, **kwargs):
        super(TimeTable, self).__init__(**kwargs)

        self.orientation = 'vertical'

        self.app = App.get_running_app()
        self.rows = self.app.config_settings.getint('schedule', 'coursesaday')

        self.tt = dict()

        timetable = self.app.dbh.get_timetable_data()
        for i in range(self.rows):
            self.tt[i] = BoxLayout(orientation='horizontal')
            self.tt[i].add_widget(Label(text='[b]{}[/b]'.format(i + 1), markup=True))

            try:
                self.tt[i].add_widget(TextInput(text=timetable[i][0], multiline=False))
            except (KeyError, IndexError):
                self.tt[i].add_widget(TextInput(text='', multiline=False))
            try:
                self.tt[i].add_widget(TextInput(text=timetable[i][1], multiline=False))
            except (KeyError, IndexError):
                self.tt[i].add_widget(TextInput(text='', multiline=False))

            self.add_widget(self.tt[i])
        self.add_widget(Button(text=cap_first_letter(_(strings.text['close'])), on_release=self.on_close))

    def on_close(self, *args, **kwargs):
        # check weeks
        weeks_popup = Popup(title='Weeks for schedule', auto_dismiss=False, size_hint=(.7, .5))
        weeks_popup.attach_to = self
        weeks_popup.content = CreateActivities()
        weeks_popup.open()


class CreateActivities(BoxLayout):
    def __init__(self, **kwargs):
        super(CreateActivities, self).__init__(**kwargs)

        self.app = App.get_running_app()

        self.orientation = 'vertical'

        self.weeks_input = TextInput(text='', multiline=False)

        self.add_widget(Label(text='Insert number of weeks to use this schedule'))
        self.add_widget(self.weeks_input)
        self.add_widget(Button(text=cap_first_letter(_(strings.text['close'])), on_release=self.on_close))

    def on_close(self, *args, **kwargs):
        try:
            weeks = int(self.weeks_input.text)
            if weeks < 1:
                raise ValueError
        except ValueError:
            popup = Popup(title='', content=Label(text='Invalid number of weeks (min. 1)'), size_hint=(.8, .4))
            popup.attach_to = self
            popup.open()
        else:
            weeks_popup = self.parent.parent.parent
            timetable_dialog = weeks_popup.attach_to.parent.parent.parent
            weeks_popup.dismiss()

            schedule_dialog = timetable_dialog.attach_to

            # save timetable
            self.app.dbh.timetable_to_db(weeks_popup.attach_to.tt)   # TODO: Check user input

            timetable_dialog.dismiss()

            # save schedule
            courses = schedule_dialog.ids['schedule_layout'].__self__.courses
            tto = schedule_dialog.ids['schedule_layout'].__self__.time_table_objects
            self.app.dbh.schedule_to_db(courses, tto)

            # push activity for n weeks to database
            self.app.dbh.schedule_to_activity(weeks)

            schedule_dialog.parent.parent.parent.dismiss()